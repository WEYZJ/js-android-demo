package com.demo.mywebviewtest;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class FarmActivity extends AppCompatActivity {

    private WebView webview;
    private Button get;
    private JsInterface mJsInterface;
    private   String HOME_URL="http://farm.soumingyan.com/shentou/index.html";

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
         get = (Button) findViewById(R.id.android_btn);
         webview = (WebView) findViewById(R.id.android_web);




        mJsInterface = new JsInterface();


        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);   //设置WebView可以与JS交互 这里必须设置
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);   //设置允许JS中的弹窗
        // 编码方式
        webSettings.setDefaultTextEncodingName("utf-8");
        //jsObj只是一个桥接对象，可以任意定义
        webview.addJavascriptInterface(mJsInterface, "jsObj");
        //加载assets下面的html页面
        //webview.loadUrl(HOME_URL);

        webview.loadUrl("file:///android_asset/test.html");
        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webview.loadUrl("javascript: clickJSTwo(‘我是传递的什么东西’)");
            }
        });
    }

    private class JsInterface {
        /**
         * js中通过window.jsObj.javaMethod("参数") 可以调用此方法并且把js中input中的值作为参数传入，
         * 必须加标注@JavascriptInterface
         * @param param
         */
        @JavascriptInterface
        public void androidMethod(String param) {
            Log.e("eicky", "我是js里面传过来的参数:" + param);
            Toast.makeText(FarmActivity.this, "我是js里面传过来的参数:" + param, Toast.LENGTH_SHORT).show();
        }
    }
}
